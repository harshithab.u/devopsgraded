package com.gl.hellodevops.HelloDevOps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.boot.CommandLineRunner;

@SpringBootApplication
@RestController
public class HelloDevOpsApplication extends SpringBootServletInitializer implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(HelloDevOpsApplication.class, args);
		System.out.println("Welcome Dev-Ops");
	}

@Override 
public void run(String... args) throws Exception{ 
	System.out.println("Hello Dev-Ops"); 
	}

	@Override
	 protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	  return application.sources(HelloDevOpsApplication.class);
	 }

	
    @RequestMapping("/")	
	public String helloDevOps() {
		return "Welcome Dev-Ops";
		
	}
	

}
